import './App.css';
import Home from './component/Home';
import Navbar from './component/Navbar';
import {Switch, Route} from 'react-router-dom'
import Products from './component/Products';
import Product from './component/Product';
import Cart from './component/Cart';
import AboutUs from './component/AboutUs';
import Contact from './component/Contact';
import { UserProvider } from './context/userContext';
import Dasboard from './component/Dasboard';
import Add from './component/Add';
import Update from './component/Update';
import Edit from './component/Edit';
import Checkout from './component/Checkout';
import Order from './component/Order';
 
function App() {
  return (
 <UserProvider>
 <Navbar/>
 <Switch>
   <Route exact path='/' component={Home} />
   <Route exact path='/products' component={Products} />
   <Route exact path='/products/:id' component={Product} />
   <Route exact path='/cart' component={Cart} />
   <Route exact path='/about' component={AboutUs} />
   <Route exact path='/contact' component={Contact} />
   <Route exact path='/dasboard' component={Dasboard} />
   <Route exact path='/add' component={Add} />
   <Route exact path='/update/' component={Update} />
   <Route exact path='/update/:id/edit' component={Edit} />
   <Route exact path='/checkout' component={Checkout} />
   <Route exact path='/order' component={Order} />

   <Home />
 </Switch>
 
 </UserProvider>
  );
}

export default App;
