import React from 'react'
import LoginG from '../component/login'
import { useEffect, useContext, useState } from 'react'
import {UserContext} from "../context/userContext"
import axios from "axios"
import {Redirect, Route, Link} from 'react-router-dom'
import { gapi } from 'gapi-script'


const clientId ="965657828139-3k87vahsm8j52oovmc8tt4ufurhptus9.apps.googleusercontent.com"

const Login = () => {
    const [, setUser] = useContext(UserContext)
    const [input, setInput] = useState({email: "" , password: ""})
    const MINUTE_MS = 4000;


    useEffect(()=>{
        function start() {
            gapi.client.init({
                clientId: clientId,
                scope: ""
            })
        }
        gapi.load("client:auth2", start)
    })
    const [user] = useContext(UserContext)


    const LoginRoute = ({...rest}) =>{
        if (user){
          return <Redirect to="/" />
        }else{
          return <Route {...rest}/>
        }
    }

    const handleSubmit = (event) =>{
        event.preventDefault()
        axios.post("https://second-backend-heroku.herokuapp.com/login", {
          username: input.username, 
          password: input.password
        }).then(
          (res)=>{
            var user = res.data.user
            var token = res.data.token
            var currentUser = {username: user.username, email: user.email, token }
            setUser(currentUser)
            localStorage.setItem("user", JSON.stringify(currentUser))
          }
        ).catch((err)=>{
          alert(err)
        })
      }
    
      const handleChange = (event) =>{
        let value = event.target.value
        let name = event.target.name
        setInput({...input, [name]: value})
      }

      const myFunction = (event) => {
        alert("Login Successs!");
        const timerId = setTimeout(() => {
          event = window.location.reload();
        
        }, MINUTE_MS);
      
        
        return () => clearTimeout(timerId)

      }
    
    
    return (
        <>
            {/* <!-- Button trigger modal --> */}
            
            { user === null && (
           <LoginRoute>
           <button type="button" className='btn btn-outline-dark ms-auto' data-bs-toggle="modal" data-bs-target="#loginModal">
                <i className="fa fa-sign-in me-1"></i>Login
            </button>
            </LoginRoute>
           )}
           { user !== null && (
                <Link to="/dasboard">
                
                    <button class="btn btn-outline-dark ms-2"><i className="fa fa-home  me-1"></i> My Dasboard</button>
            </Link>
           )}
            
            {/* <!-- Modal --> */}
            <div className="modal fade" id="loginModal" tabIndex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div className="modal-dialog">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title" id="exampleModalLabel">Login</h5>
                            <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div className="modal-body">
                            <div>
                               <span className=' me-2'></span><LoginG/> 
                            </div>
                            <form onSubmit={handleSubmit}>
                            <div className="mb-3">
                                    <label htmlFor="exampleInput" className="form-label">Username</label>
                                    <input type="text" name='username' onChange={handleChange} value={input.nama} className="form-control" id="exampleInput"/>
                                </div>
                                <div className="mb-3">
                                    <label htmlFor="exampleInputPassword1" className="form-label">Password</label>
                                    <input type="password" name='password' onChange={handleChange} value={input.password} className="form-control" id="exampleInputPassword1"/>
                                </div>
                                <div className="mb-3 form-check">
                                    <input type="checkbox" className="form-check-input" id="exampleCheck1"/>
                                        <label className="form-check-label" htmlFor="exampleCheck1">Check me out</label>
                                </div>
                                <button onClick={myFunction} data-bs-toggle="modal" data-bs-target="#addLogin" type="submit" className="btn btn-outline-dark w-100 mt-4" >Submit</button>
                                {/* <!-- Modal --> */}
                                
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}

export default Login