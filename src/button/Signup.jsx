import LoginG from '../component/login'
import React, { useContext, useState } from "react"
import {UserContext} from "../context/userContext"
import axios from "axios"
import {Redirect, Route} from 'react-router-dom'
    


const Signup = () => {
    
    const [, setUser] = useContext(UserContext)
    const [input, setInput] = useState({username: "", email: "" , password: ""})
    const MINUTE_MS = 5000;

  
    const handleSubmit = (event) =>{
      event.preventDefault()
      axios.post("https://second-backend-heroku.herokuapp.com/register", {
        username: input.username, 
        email: input.email, 
        password: input.password
      }).then(
        (res)=>{
          var user = res.data.user
          var token = res.data.token
          var currentUser = {username: user.username, email: user.email, token }
          setUser(currentUser)
          localStorage.setItem("user", JSON.stringify(currentUser))
        }
      ).catch((err)=>{
        alert(err)
      })
    }
  
    const handleChange = (event) =>{
      let value = event.target.value
      let name = event.target.name
      setInput({...input, [name]: value})
    }

    const [user] = useContext(UserContext)
  const PrivateRoute = ({...rest}) =>{
    if (user){
      return <Route {...rest}/>
    }else{
      return <Redirect to="/login" />
    }
  }

  const LoginRoute = ({...rest}) =>{
    if (user){
      return <Redirect to="/" />
    }else{
      return <Route {...rest}/>
    }
  }
  const myFunction = (event) => {
    alert("Login Successs!");
    const timerId = setTimeout(() => {
      event = window.location.reload();
    
    }, MINUTE_MS);
  
    
    return () => clearTimeout(timerId)

  }

  
    return (
        <>
           {/* <!-- Button trigger modal --> */}
           { user === null && (
           <LoginRoute>
           <button type="button" className='btn btn-outline-dark ms-2' data-bs-toggle="modal" data-bs-target="#signupModal">
                <i className="fa fa-user-plus me-1"></i>Register
            </button>
            </LoginRoute>
           )}
           { user !== null && (
           
           <button type="button" className='btn btn-outline-dark ms-2' onClick={()=>{setUser(null)
            localStorage.clear().window.location.reload() 
          }}>
            
                <i className="fa fa-sign-out me-1"></i>Logout
            </button>
    
           )}

            {/* <!-- Modal --> */}
            <div className="modal fade" id="signupModal" tabIndex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div className="modal-dialog">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title" id="exampleModalLabel">Register</h5>
                            <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div className="modal-body">
                        <div>
                               <span className=' me-2'></span><LoginG/> 
                            </div>
                            {/* <div className="btn btn-outline-dark w-100 mb-4">
                               <span className='fa fa-facebook me-2'></span> Sign up With Facebook
                            </div> */}
                            <form onSubmit={handleSubmit}>
                                <div className="mb-3">
                                    <label htmlFor="exampleInput" className="form-label">Username</label>
                                    <input type="text" name='username' onChange={handleChange} value={input.nama} className="form-control" id="exampleInput"/>
                                </div>
                                <div className="mb-3">
                                    <label htmlFor="exampleInputEmail1" className="form-label">Email address</label>
                                    <input type="email" name='email' onChange={handleChange} value={input.email} className="form-control" id="exampleInputEmail1" aria-describedby="emailHelp"/>
                                        <div id="emailHelp" className="form-text">We'll never share your email with anyone else.</div>
                                </div>
                                <div className="mb-3">
                                    <label htmlFor="exampleInputPassword1" className="form-label">Password</label>
                                    <input type="password" name='password' onChange={handleChange} value={input.password} className="form-control" id="exampleInputPassword1"/>
                                </div>
                                <div className="mb-3 form-check">
                                    <input type="checkbox" className="form-check-input" id="exampleCheck1"/>
                                        <label className="form-check-label" htmlFor="exampleCheck1">Check me out</label>
                                </div>
                                <button type="submit" className="btn btn-outline-dark w-100 mt-4" >Register</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}

export default Signup