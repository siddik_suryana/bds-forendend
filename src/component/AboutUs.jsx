import React from 'react'
import { NavLink } from 'react-router-dom'


const AboutUs = () => {
    return (
        <div> 
            <div className='container'>
                <div className="row">
                    <div className="col-md-5">
                        <h1 className='text-primary fw-bold my-4'>About Us</h1>
                        <p className='lead'>
                        <p><span className='text-secondary fw-bold'>SUN COLLECTION</span> E-commerce is the buying and selling of goods or services via the internet, and the transfer of money and data to complete the sales. It’s also known as electronic commerce or internet commerce.</p>
                        <p> Online selling has changed tremendously since it began; the evolution and history of e-commerce is fascinating – and it’s advancing at an even quicker pace today.</p>
                        <p>Today, questions about e-commerce usually center around which channels are best to execute business online, but one of the most burning questions is the appropriate spelling of e-commerce. The truth is, there isn’t any one that’s right or wrong, and it usually comes down to preference.</p>
                        </p>
                    </div>
                    <div className='col-md-6'>
                        <img src='/assets/aboutus.png' className='my-5' style={{width:"130%"}}></img>

                    </div>
                        <NavLink to="/contact" className="btn btn-outline-dark mb-5 mx-auto" style={{width:"25%"}}>Contact Us</NavLink>
                </div>
            </div>
            <footer class="py-3 my-4">
    <ul class="nav justify-content-center border-bottom pb-3 mb-3">
      <li class="nav-item"><a href="#" class="nav-link px-2 text-muted">Home</a></li>
      <li class="nav-item"><a href="#" class="nav-link px-2 text-muted">Product</a></li>
      <li class="nav-item"><a href="#" class="nav-link px-2 text-muted">Pricing</a></li>
      <li class="nav-item"><a href="#" class="nav-link px-2 text-muted">FAQs</a></li>
      <li class="nav-item"><a href="#" class="nav-link px-2 text-muted">About</a></li>
    </ul>
    <p class="text-center text-muted">&copy; SunColleciton 2022, "Shop Forever Growth Together" </p>
  </footer>
        </div>
    )
}

export default AboutUs