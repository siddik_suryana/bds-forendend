import React, { useEffect, useState, useContext } from 'react'
import { UserContext } from '../context/userContext';
import Skeleton from "react-loading-skeleton"
import 'react-loading-skeleton/dist/skeleton.css'
import { useHistory, Link, useParams } from 'react-router-dom';
import NumberFormat from 'react-number-format';
import axios from 'axios';


const Add = () => {
  let { id } = useParams();
  let initialForm = { title: '', price: '', category: '', Rating: '', image: '', description: '' }
  let history = useHistory();
  const [currentId, setCurrentId] = useState(null)
  const [user] = useContext(UserContext)
  const [input, setInput] = useState(initialForm)
  // const [data, setData] = useState([]);
  // const [filter, setFilter] = useState(data);

  useEffect(() => {
    const fetchData = async () => {
      const result = await axios.get(`https://second-backend-heroku.herokuapp.com/shop/${id}`)
      let { title, price, category, Rating, image, description } = result.data
      setInput({ title, price, category, Rating, image, description })
      setCurrentId(id)
      

    }

    if (id !== undefined) {
      fetchData()
    }
  }, [id])

  // const ActionButton = ({ name, id }) => {

  //     const handleAction = () => {
  //         let caseName = name.toLowerCase()
  //         if (caseName === "delete") {

  //             axios.delete(`https://second-backend-heroku.herokuapp.com/shop/${id}`, { headers: { "Authorization": "Bearer " + user.token } })
  //                 .then(() => {
  //                     setFetchTrigger(true)



  //                     // if (form.currentId !== null){
  //                     //   setForm({...form, currentId: null})
  //                     // }
  //                 }

  //                 )
  //                 .catch((err) => console.log("Err", err))
  //         } else if (caseName === "edit") {
  //             history.push(`/movies-list/${id}/edit`)
  //             console.log("edit masuk", id)
  //         }
  //     }
  //     return (
  //         <button onClick={handleAction}>{name}</button>
  //     )
  // }

  const handleSubmit = (event) => {
    event.preventDefault()
    if (currentId !== null) {
      // edit
      axios.put(`https://second-backend-heroku.herokuapp.com/shop/${currentId}`, input, { headers: { "Authorization": "Bearer " + user.token } })
        .then(() => {
          history.push("/shop/:id/edit")

        })
    } else {
      //   // new
      axios.post(`https://second-backend-heroku.herokuapp.com/shop/`, input, { headers: { "Authorization": "Bearer " + user.token } })
      .then(() => {
          history.push("/add")
        })
    }
    setInput(initialForm)
  }
  
  const price = parseInt(input.price)
  
  
  const handleChange = (event) => {
    let value = event.target.value
    let name = event.target.name
    switch (name){
      case "title":{
        setInput({...input, title: value})
        break;
      }
      case "price":{
        setInput({...input, price: parseInt(value)})
        break;
      }
      case "category":{
        setInput({...input, category: value})
        break;
      }
      case "image":{
        setInput({...input, image: value})
        break;
      }
      case "description":{
        setInput({...input, description: value})
        break;
      }
      case "Rating":{
        setInput({...input, Rating: value})
        break;
      }
      default:{break;}
    }
  }
  


  return (
    <div class="container-fluid">
      <div class="row">
        <nav id="sidebarMenu" class="col-md-3 col-lg-2 d-md-block bg-light sidebar collapse">
          <div class="position-sticky pt-3">
            <ul class="nav flex-column">
            <li class="nav-item">
                                    <a class="nav-link " aria-current="page" href="#">
                                        <span data-feather="home"></span>
                                        <Link to="/order">
                                            <button class="fa fa-shopping-bag btn-outline-dark py-2 ms-auto w-100"> Order</button>
                                        </Link>
                                    </a>
                                </li>
              <li class="nav-item">
                <a class="nav-link " aria-current="page" href="#">
                  <span data-feather="home"></span>
                  <Link to="/dasboard">
                    <button class="fa fa-minus btn btn-outline-dark ms-auto w-100"> Delete Products</button>
                  </Link>
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link active" aria-current="page" href="#">
                  <span data-feather="home"></span>
                  <Link to="/add">
                    <button class="fa fa-plus btn btn-outline-dark ms-auto w-100"> Add Products</button>
                  </Link>
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link active" aria-current="page" href="#">
                  <span data-feather="home"></span>
                  <Link to="/update">
                    <button class="fa fa-refresh btn btn-outline-dark ms-auto w-100"> Update Products</button>
                  </Link>
                </a>
              </li>

            </ul>
          </div>
        </nav>

        <main class="col-md-9 ms-sm-auto col-lg-10 px-md-4">
          <div>
            <div className="container my-5 py-5">
              <div className="row">
                <div className="col-12 mb-5" >
                  <h1 className='display-6 fw-bolder text-center' >Add Some Products</h1>
                  <hr />
                </div>
              </div>
              <form onSubmit={handleSubmit}>
                <div class="row g-3">
                  <div class="col-sm-6">
                    <label for="title" class="form-label">Product Name</label>
                    <input type="text" name="title" class="form-control" id="title" onChange={handleChange} value={input.title} placeholder="Name" />
                  </div>

                  <div class="col-sm-6">
                    <label for="parse1" class="form-label">Product Price</label>
                    <input type="number" name="price" class="form-control" id="typeNumber" onChange={handleChange} value={price} placeholder="Price" required />
                    <div class="invalid-feedback">
                      Valid Price required.
                    </div>
                  </div>

                  <div class="col-sm-6">
                    <label for="category" class="form-label">Product Category</label>
                    <input type="text" name="category" class="form-control" id="category" onChange={handleChange} value={input.category} placeholder="Category" required />
                    <div class="invalid-feedback">
                      Valid Category is required.
                    </div>
                  </div>

                  <div class="col-sm-6">
                    <label for="Rating" class="form-label">Product Rating</label>
                    <input type="text" name="Rating" class="form-control" id="Rating" onChange={handleChange} value={input.Rating} placeholder="Rating" required />
                    <div class="invalid-feedback">
                      Valid Rating is required.
                    </div>
                  </div>

                  <div class="col-12">
                    <label for="image" class="form-label">Image Url</label>
                    <div class="input-group has-validation">
                      <input type="text" class="form-control" name="image" id="image" onChange={handleChange} value={input.image} placeholder="Url" required />
                      <div class="invalid-feedback">
                        Your image is required.
                      </div>
                    </div>
                  </div>

                  <div class="col-12">
                    <label for="description" class="form-label">Description </label>
                    <textarea name='description' class="form-control" id="description" onChange={handleChange} value={input.description} rows="3"></textarea>
                    <div class="invalid-feedback">
                      Please enter a valid description.
                    </div>
                  </div>

                  <button data-bs-toggle="modal" data-bs-target="#addModal" class="btn btn-outline-dark col-2 my-5 ms-2" type="submit">Submit</button>
                  {/* <!-- Modal --> */}
                  <div className="modal fade" id="addModal" tabIndex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div className="modal-dialog">
                      <div className="modal-content">
                        <div className="modal-header">
                        <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div className="modal-body">
                        <img src='/assets/checklist.png' style={{width:"100%"}}></img>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </form>

            </div>
            {/* footer */}
            <div class="container">
              <footer class="py-3 my-4">
                <ul class="nav justify-content-center border-bottom pb-3 mb-3">
                  <li class="nav-item"><a href="#" class="nav-link px-2 text-muted">Home</a></li>
                  <li class="nav-item"><a href="#" class="nav-link px-2 text-muted">Product</a></li>
                  <li class="nav-item"><a href="#" class="nav-link px-2 text-muted">Pricing</a></li>
                  <li class="nav-item"><a href="#" class="nav-link px-2 text-muted">FAQs</a></li>
                  <li class="nav-item"><a href="#" class="nav-link px-2 text-muted">About</a></li>
                </ul>
                <p class="text-center text-muted">&copy; SunColleciton 2022, "Shop Forever Growth Together" </p>
              </footer>
            </div>
          </div>

        </main>
      </div>
    </div>
  )
}

export default Add