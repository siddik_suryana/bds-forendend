import React, { useEffect, useState, useContext } from 'react'
import { useParams } from 'react-router-dom';
import { useSelector } from "react-redux";
import { useDispatch } from "react-redux";
import { addCart, delCart, minCart } from "../redux/action/index"
import NumberFormat from 'react-number-format';
import axios from 'axios';

import Product from "./Product";
import { Link } from "react-router-dom";
import { NavLink } from "react-router-dom";
import { UserContext } from '../context/userContext';

const Cart = () => {
    let { id } = useParams();
    const [data, setData] = useState([]);
    const [filter, setFilter] = useState(data);
    const [user] = useContext(UserContext)
    const [fetchTrigger, setFetchTrigger] = useState(true)
    const state = useSelector((state) => state.handleCart)
    const dispatch = useDispatch()
    const [currentId, setCurrentId] = useState(null)


    let componentMounted = true;

    useEffect(() => {
        const fetchData = async () => {

            // setLoading(true)
            // const result = await axios.get(`https://backend-fullstack-heroku.herokuapp.com/data-shopping/current-user`, { headers: { "Authorization": "Bearer " + user.token } })
            // let shoppings = result.data
            //-----ngeluarin status-----
            // let findShoppingCart = shoppings.find((item) => {
            //     return item.status == "cart"
            // })
            //---- validasi status ----
            // if (findShoppingCart != undefined) {
            //     user.shoppingID = findShoppingCart.id
            // } else {
            //     user.shoppingID = null
            // }
            // const resnponse = await axios.get(`https://backend-fullstack-heroku.herokuapp.com/data-shopping/${user.shoppingID}/data-cart`)
            // const data_cart = resnponse.data
            // setFilter(data_cart)
            // if (componentMounted) {
            //     setData(await resnponse.clone().json());
            //     setFilter(await resnponse.json());
            // setLoading(false);
            // console.log(filter)
            // }
            // return () => {
            //     componentMounted = false;
            // }
            const result = await axios.get(`https://second-backend-heroku.herokuapp.com/cart-current-user`, { headers: { "Authorization": "Bearer " + user.token } })
            let data_cart = result.data
            setFilter(data_cart)
            setCurrentId(id)
            

        }
        if (fetchTrigger) {

            if (user.shoppingID == null) {

                fetchData()
            }
            setFetchTrigger(false)
        }
        //bootom : , user
    }, [fetchTrigger])

    // const handlePlus = (item) =>{
    //     dispatch(addCart(item))
    // }
    // const handleMinus = (item) =>{
    //         dispatch(minCart(item))
    // }
    const handleClose = ({id}, refresht) =>{
        
        console.log(id)

        axios.delete(`https://second-backend-heroku.herokuapp.com/cart/${id}`, {headers: {"Authorization" : "Bearer "+ user.token}})
            .then(() => {
              setFetchTrigger(true)
              refresht = window.location.reload();
            }

            )
    }

    // const handleSubmit = async (event) => {
    //     // window.location.reload();
    //     if (user.shoppingID == null) {
    //         // post

    //         const shopping = await axios.post(`https://backend-fullstack-heroku.herokuapp.com/data-shopping/`, {}, { headers: { "Authorization": "Bearer " + user.token } })//input speart

    //     } else if (user.shoppingID != null){
    //         const shopping = await axios.post(`https://backend-fullstack-heroku.herokuapp.com/data-shopping/`, {}, { headers: { "Authorization": "Bearer " + user.token } })//input speart
    //         const cart = await axios.post(`https://backend-fullstack-heroku.herokuapp.com/data-cart/`, { structShoppingId: shopping.data.id, quantity: 1, structProductId: parseInt(id) }, { headers: { "Authorization": "Bearer " + user.token } })
    //     }
    //     fetchTrigger(false)

    // }

    const emptyCart = () => {
        return (
            <div className="px-4 my-5 bg-light rounded-3 py-5">
                <div className="container py-4">
                    <div className="row">
                        <h3>Your Cart is Empty</h3>
                    </div>
                </div>
            </div>
        )
    }

    return (
        <div className="container">

            <div className="col-12 mb-5 pt-5" >
                <h1 className='display-6 fw-bolder text-center' >Cart List</h1>
                <hr />
            </div>
            {/* {user.shoppingID === null && emptyCart()} 
            bottom : filter.length !== 0 && 
            */}
            {filter.map((item) => {
                return (
                    <div>

                        <div className="px-4 my-4 bg-light rounded-3" key={''}>
                            <div className="container py-3">
                                {/* bottom onClick={(cek)=> handleClose(item, cek)} */}
                                <button className="btn btn-outline-dark float-end" aria-label="Close" onClick={(cek)=> handleClose(item, cek)}><i className="fa fa-times" /></button>
                                <div class="row">


                                    <div className="col-md-3">
                                        <img src={item.StructShop.image} alt={item.StructShop.title} height="200px" width="180px" />

                                    </div>
                                    <div className="col-md-7">
                                        <h3>{item.StructShop.title}</h3>
                                        <h4 className='my-5 float-end'><i className=" fa fa-check-square-o" />
                                            Rp <NumberFormat value={item.StructShop.price} displayType={'text'} thousandSeparator={true} prefix={' '} />
                                        </h4>
                                    </div>

                                    <div className="col-md-4">

                                        {/* <button className="btn btn-outline-dark me-4"
                                onClick={()=> handleMinus(cartItem)} 
                                >
                                    <i className="fa fa-minus"></i>
                                </button>
                                <button className="btn btn-outline-dark"
                                onClick={()=> handlePlus(cartItem)} >
                                    <i className="fa fa-plus"></i>
                                </button> */}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                )
            })}
            <div className="container">
             {/* bottom :onSubmit={handleSubmit} */}
        <form>                               
                

                    <NavLink to="/checkout" className="btn btn-outline-dark mb-5 mx-auto" style={{ width: "15%" }} type="submit">Checkout</NavLink>
                
                  </form>
            </div>
            <footer class="py-3 my-4">
                <ul class="nav justify-content-center border-bottom pb-3 mb-3">
                    <li class="nav-item"><a href="#" class="nav-link px-2 text-muted">Home</a></li>
                    <li class="nav-item"><a href="#" class="nav-link px-2 text-muted">Product</a></li>
                    <li class="nav-item"><a href="#" class="nav-link px-2 text-muted">Pricing</a></li>
                    <li class="nav-item"><a href="#" class="nav-link px-2 text-muted">FAQs</a></li>
                    <li class="nav-item"><a href="#" class="nav-link px-2 text-muted">About</a></li>
                </ul>
                <p class="text-center text-muted">&copy; SunColleciton 2022, "Shop Forever Growth Together" </p>
            </footer>
        </div>
      

    )

}

export default Cart