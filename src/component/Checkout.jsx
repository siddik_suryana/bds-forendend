import React, { useEffect, useState, useContext } from 'react'
import { useParams } from 'react-router-dom';
import { UserContext } from '../context/userContext';
import axios from 'axios';
import { useSelector } from 'react-redux'
import NumberFormat from 'react-number-format';
import { useRef } from 'react';
import emailjs from '@emailjs/browser';
import { Link } from "react-router-dom";




const Checkout = () => {
    let { id } = useParams();
    const [data, setData] = useState([]);
    const [filter, setFilter] = useState(data);
    const [user] = useContext(UserContext)
    const [fetchTrigger, setFetchTrigger] = useState(true)

    // const [loading, setLoading] = useState(false);
    // const state = useSelector((state)=> state.handleCart)
    // const dispatch = useDispatch()
    let componentMounted = true;

    useEffect(() => {
        const fetchData = async () => {
            // setLoading(true)
            const resnponse = await fetch(`https://second-backend-heroku.herokuapp.com/cart-current-user`, { headers: { "Authorization": "Bearer " + user.token } })
            if (componentMounted) {
                setData(await resnponse.clone().json());
                setFilter(await resnponse.json());
                // setLoading(false);
                console.log(resnponse)
                
            }
            return () => {
                componentMounted = false;
            }
            
        }
        if (fetchTrigger) {
            fetchData()
            setFetchTrigger(false)
        }
       
    },[fetchTrigger])
    const state = useSelector((state)=> state.handleCart)
    var total = 0
    // const itemList = (item) => {
    //     total = total + item.price
    //     return(
    //         <li className="list-group-item d-flex justify-content-between lh-lg">
    //                         <div>
    //                             <h6 className="my-2 mx-3">{item.title}</h6>
    //                         </div>
    //                         <span className="text-muted col-3">Rp<NumberFormat value={item.price} displayType={'text'} thousandSeparator={true} prefix={' '} /></span>
    //         </li>
    //     )
    // }
    const form = useRef();
    const sendEmail = (e) => {
        e.preventDefault();

        emailjs.sendForm('service_png5xxb', 'template_s4mnhjh', form.current, 'mliaFU7CSVdQfdO2M')
            .then((result) => {
                console.log(result.text);
            }, (error) => {
                console.log(error.text);
            });
            e.target.reset()
    }
    
    const handleClose = ({id}, refresh) =>{
        axios.delete(`https://backend-fullstack-heroku.herokuapp.com/shopping/${id}`, {headers: {"Authorization" : "Bearer "+ user.token}})
            .then(() => {
              setFetchTrigger(true)
              refresh = window.location.reload()
            }
            
            )
    }
    const emptyCart = () => {
        return(
            <div className="px-4 my-3 bg-light rounded-3 py-5">
                <div className="container py-4">
                    <div className="row">
                        <h3>Your Cart is Empty</h3>
                    </div>
                </div>
            </div>
        )
    }

    const add = async (event) => {
        // event.preventDefault()
        const total = total + filter.StructShop.price      
            // post
            const cart = await axios.post(`http://second-backend-heroku.herokuapp.com/transaction`, { username_transaction: filter.user.username, status_transaction:"unpaid", user_id:filter.id_user, price_transaction:1}, { headers: { "Authorization": "Bearer " + user.token } })
            // event = window.location.reload();
            fetchTrigger(false)

        

        
    }
      
    return (
        <>
        {console.log(filter)}
        <div className="container">
            <div className="row g-5 my-4">
                <div className="col-md-7 col-lg-6 order-md-last">
                    <h4 className="d-flex justify-content-between align-items-center mb-3">
                        <span className="text-dark">Your cart</span>
                        <span className="badge bg-dark rounded-pill">{filter.length}</span>
                        
                    </h4>
                    <hr></hr>
                    <ul className="list-group mb-3">
            {filter.length === 0 && emptyCart()}
                    {filter.map((items) => {
                        total = total + items.StructShop.price
                        return (
                        <li className="list-group-item d-flex justify-content-between lh-lg">
                        <div className="row  my-2">
                        <div>
                            <h6 className="col-md-5 col-lg-12">{items.StructShop.title}</h6>
                        </div>
                        <span className="text-muted col-md-5 col-lg-6">Rp<NumberFormat value={items.StructShop.price} displayType={'text'} thousandSeparator={true} prefix={' '} /></span>
                            </div>    
                        <button onClick={(e)=> handleClose(items, e)} className="btn btn-outline-dark fa fa-times mt-1 mb-5 px-2 float-end" aria-label="Close"></button>
                        </li>
                    )})}
                        <div className="d-flex justify-content-between">
                            
                            <span>--------------------------------------------------------------------------------</span>
                            <span>+</span>
                        </div> 
                        <li className="list-group-item d-flex justify-content-between">
                            <span className='mx-3'>Total (Rp)</span>
                            <strong className='col-3'>Rp <NumberFormat value={total} displayType={'text'} thousandSeparator={true} prefix={' '} /></strong>
                        </li>
                    </ul>
                </div>
                <div className="col-md-5 col-lg-6">
                    <h4 className="mb-3">Billing address</h4>
                    <form className="needs-validation" novalidate="" ref={form} onSubmit={add}>
                        <div className="row g-3">
                            <div className="col-12">
                                <label for="username" className="form-label">Fullname</label>
                                    <input type="text" className="form-control" id="username" placeholder="Jhon Doe" name="name" required="" />
                                    <div className="invalid-feedback">
                                        Your username is required.
                                    </div>
                        
                            </div>

                            <div className="col-12">
                                <label for="email" className="form-label">Email</label>
                                <input type="email" className="form-control" id="email" placeholder="jhondoe@gmail.com" name="email" />
                                <div className="invalid-feedback">
                                    Please enter a valid email address for shipping updates.
                                </div>
                            </div>

                            <div className="col-12">
                                <label for="address" className="form-label">Address</label>
                                <input type="text" className="form-control" id="address" placeholder="Gedung Pakuan No.1 Kota Bandung" name="address" required="" />
                                <div className="invalid-feedback">
                                    Please enter your shipping address.
                                </div>
                            </div>
                            <div className="col-12">
                                <label for="hp" className="form-label">Hp</label>
                                <input type="defaultValue" className="form-control" id="hp" placeholder="0897653265" name="hp" required="" />
                                <div className="invalid-feedback">
                                    Please enter your shipping address.
                                </div>
                            </div>
                            <div className="col-12">
                                <label for="total" className="form-label">Total</label>
                                <input type="defaultValue" className="form-control" id="total" placeholder={total} value={total} name="total" required="" />
                                <div className="invalid-feedback">
                                    Please enter your shipping address.
                                </div>
                            </div>
                            
                        </div>
                        <div class="my-3">
                        <h4 class="mt-5 mb-3"><i class="fa fa-credit-card-alt" /> Payment</h4>
                        <hr />
            <div class="form-check">
              <input id="BCA" name="paymentMethod" type="radio" class="mx-2" required=""/> 
              <label class="form-check-label" for="BCA"><img src="/assets/bca.png"  alt="Background" width={"75px"} /> 7771112909 ( A.n SUNCOLLECTION ) </label>
            </div>
            <div class="form-check">
              <input id="BJB" name="paymentMethod" type="radio" class="mx-2" required=""/>
              <label class="form-check-label" for="BJB"><img src="/assets/bjb.png"  alt="Background" width={"75px"} /> 7771112909 ( A.n SUNCOLLECTION )</label>
            </div>
            <div class="form-check">
              <input id="BNI" name="paymentMethod" type="radio" class="mx-2" required=""/>
              <label class="form-check-label" for="BNI"><img src="/assets/bni.png"  alt="Background" width={"75px"} /> 7771112909 ( A.n SUNCOLLECTION )</label>
            </div>
            <div class="form-check">
              <input id="BRI" name="paymentMethod" type="radio" class="mx-2" required=""/>
              <label class="form-check-label" for="BRI"><img src="/assets/bri.png"  alt="Background" width={"65px"} /> 7771112909 ( A.n SUNCOLLECTION )</label>
            </div>
            <div class="form-check">
              <input id="BSI" name="paymentMethod" type="radio" class="mx-2" required=""/>
              <label class="form-check-label" for="BSI"><img src="/assets/bsi.png"  alt="Background" width={"105px"} /> 7771112909 ( A.n SUNCOLLECTION )</label>
            </div>
          </div>

                        <button className="w-100 btn btn-outline-dark btn-lg my-4" onClick={add}  type="submit">Submit</button>

         
       
                    </form>
                </div>
            </div>
            </div>
        
        </>

    )
}

export default Checkout