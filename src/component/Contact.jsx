import React from 'react'
import { useRef } from 'react';
import emailjs from '@emailjs/browser';


const Contact = () => {
    const form = useRef();
    const sendEmail = (e) => {
        e.preventDefault();

        emailjs.sendForm('service_i7w0t2q', 'template_18gjxoh', form.current, 'j4NNW2X_kGm4qOlIk')
            .then((result) => {
                console.log(result.text);
            }, (error) => {
                console.log(error.text);
            });
            e.target.reset()
    }

    return (
        <div>
            <div className='container'>
                <div className="row">
                    <div className='col-12 text-center py-4 my-4'>
                        <h1 >
                            Have Some Question ?
                        </h1>
                        <hr />
                    </div>
                </div>
                <div className="row">
                    <div className='col-md-7 d-flex justify-content-center'>
                        <img style={{ width: "100%" }} src='/assets/message1.png' />
                    </div>
                    <div className="col-md-5 mb-5">
                        <form ref={form} onSubmit={sendEmail}>
                            <div class="mb-3">
                                <label for="exampleForm" class="form-label">Full Name</label>
                                <input type="text" class="form-control" id="exampleForm" placeholder="Jhon Doe" name="name" />
                            </div>
                            <div class="mb-3">
                                <label for="exampleFormControlInput1" class="form-label">Email address</label>
                                <input type="email" class="form-control" id="exampleFormControlInput1" placeholder="name@example.com" name="email" />
                            </div>
                            <div class="mb-3">
                                <label for="exampleFormControlTextarea1" class="form-label">Write Here</label>
                                <textarea name="message" class="form-control" id="exampleFormControlTextarea1" rows="5"></textarea>
                            </div>
                            <button type="submit" value="send" className="btn btn-outline-dark mt-4" >Send Massage</button>
                        </form>
                    </div>
                </div>
            </div>
            <footer class="py-3 my-4">
    <ul class="nav justify-content-center border-bottom pb-3 mb-3">
      <li class="nav-item"><a href="#" class="nav-link px-2 text-muted">Home</a></li>
      <li class="nav-item"><a href="#" class="nav-link px-2 text-muted">Product</a></li>
      <li class="nav-item"><a href="#" class="nav-link px-2 text-muted">Pricing</a></li>
      <li class="nav-item"><a href="#" class="nav-link px-2 text-muted">FAQs</a></li>
      <li class="nav-item"><a href="#" class="nav-link px-2 text-muted">About</a></li>
    </ul>
    <p class="text-center text-muted">&copy; SunColleciton 2022, "Shop Forever Growth Together" </p>
  </footer>
        </div>
    )
}

export default Contact