import React, { useEffect, useState, useContext } from 'react'
import { UserContext } from '../context/userContext';
import axios from 'axios';
import Skeleton from "react-loading-skeleton"
import 'react-loading-skeleton/dist/skeleton.css'
import { useHistory, Link, useParams } from 'react-router-dom';
import NumberFormat from 'react-number-format';
import "../App.css"



const Dasboard = () => {
    let { id } = useParams();
    const [data, setData] = useState([]);
    const [user] = useContext(UserContext)
    const [filter, setFilter] = useState(data);
    const [loading, setLoading] = useState(false);
    let history = useHistory();
    const [fetchTrigger, setFetchTrigger] = useState(true)
      const [currentId, setCurrentId] =  useState(null)

    
    useEffect(() => {
        const fetchData = async () => {
            const result = await axios.get(`https://second-backend-heroku.herokuapp.com/shop`)
            setData(result.data.map(x=>{ return {id: x.id, title: x.title, price: x.price, category: x.category, Rating: x.Rating, image: x.image, description: x.description} }) ) 
            
          
            
        }
        if (fetchTrigger) {
            fetchData()
            setFetchTrigger(false)
        }
    }, [fetchTrigger])
    

    const Loading = () => {
        return (
            <>
                <div className='display-6 fw-bolder text-center'>
                    <Skeleton height={20} width={500} />
                </div>

                <>
                    <div className="col-md-3">
                        <Skeleton height={350} />
                    </div>
                    <div className="col-md-3">
                        <Skeleton height={350} />
                    </div>
                    <div className="col-md-3">
                        <Skeleton height={350} />
                    </div>
                    <div className="col-md-3">
                        <Skeleton height={350} />
                    </div>
                </>
            </>
        )
    }
    const filterProduct = (cat) => {
        const updateList = data.filter((x) => x.category === cat)
        setFilter(updateList)
    }
    const ShowProducts = () => {
        return (
            <>
                <div className='buttons d-flex justify-content-center mb-5 pb-5'>
                    <button className='btn btn-outline-dark me-2' onClick={() => setFilter(data)}>All</button>
                    <button className='btn btn-outline-dark me-2' onClick={() => filterProduct("men's clothing")}>Men's Clothing</button>
                    <button className='btn btn-outline-dark me-2' onClick={() => filterProduct("women's clothing")}>Woman's Clothing</button>
                    <button className='btn btn-outline-dark me-2' onClick={() => filterProduct("jewelery")}>Jewelery</button>
                    <button className='btn btn-outline-dark me-2' onClick={() => filterProduct("electronics")}>Electronic</button>
                </div>
                {data.map((data) => {
                    return (
                        <>
                            
                                <div class="card my-2 mx-2 py-4" style={{width:"14rem"}} key={data.id}>
                                    <img src={data.image} class="card-img-top" alt={data.title} height="220px" />
                                    <div class="card-body text-center">
                                        <h5 class="card-title mb-0">{data.title.substring(0, 11)}..</h5>
                                        <p class="card-text lead fw-bold">Rp<NumberFormat value={data.price} displayType={'text'} thousandSeparator={true} prefix={' '} /></p>
                                        <ActionButton class="button1" name="Delete" id={data.id}/>
                                    </div>
                                </div>
                            
                        </>
                    )
                })}
            </>
        )
    }

    const ActionButton = ({name, id})=>{
    
        const handleAction = ()=>{
          let caseName = name.toLowerCase()
          if ( caseName === "delete"){
            
            axios.delete(`https://second-backend-heroku.herokuapp.com/shop/${id}`, {headers: {"Authorization" : "Bearer "+ user.token}})
            .then(() => {
              setFetchTrigger(true)

            }
            
            )
            .catch((err) => console.log("Err", err) )
          } 
        } 
        return(
          <button onClick={handleAction}>{name}</button>
        )
      }


    return (
        <div class="container-fluid">
            <div class="row">
                <nav id="sidebarMenu" class="col-md-3 col-lg-2 d-md-block bg-light sidebar collapse">
                    <div class="position-sticky pt-3">
                    <ul class="nav flex-column">
                    <li class="nav-item">
                                    <a class="nav-link " aria-current="page" href="#">
                                        <span data-feather="home"></span>
                                        <Link to="/order">
                                            <button class="fa fa-shopping-bag btn-outline-dark py-2 ms-auto w-100"> Order</button>
                                        </Link>
                                    </a>
                                </li>
                            <li class="nav-item">
                                <a class="nav-link " aria-current="page" href="#">
                                    <span data-feather="home"></span>
                                    <Link to="/dasboard">
                                <button class="fa fa-minus btn btn-outline-dark ms-auto w-100"> Delete Products</button>
                            </Link>
                                </a>
                            </li>
                            <li class="nav-item">
                            <a class="nav-link active" aria-current="page" href="#">
                                    <span data-feather="home"></span>
                            <Link to="/add">
                                <button class="fa fa-plus btn btn-outline-dark ms-auto w-100"> Add Products</button>
                            </Link>
                                </a>
                            </li>
                            <li class="nav-item">
                            <a class="nav-link active" aria-current="page" href="#">
                                    <span data-feather="home"></span>
                            <Link to="/update">
                                <button class="fa fa-refresh btn btn-outline-dark ms-auto w-100"> Update Products</button>
                            </Link>
                                </a>
                            </li>
                            
                        </ul>

                        
                    </div>
                </nav>

                <main class="col-md-9 ms-sm-auto col-lg-10 px-md-4">
                <div>
            <div className="container my-5 py-5">
                <div className="row">
                    <div className="col-12 mb-5" >
                        <h1 className='display-6 fw-bolder text-center' >Remove Some Products</h1>
                        <hr />
                    </div>
                </div>
                <div className="row justify-content-center">
                    {loading ? <Loading /> : <ShowProducts />}
                </div>
            </div>
            <div class="container">
  <footer class="py-3 my-4">
    <ul class="nav justify-content-center border-bottom pb-3 mb-3">
      <li class="nav-item"><a href="#" class="nav-link px-2 text-muted">Home</a></li>
      <li class="nav-item"><a href="#" class="nav-link px-2 text-muted">Product</a></li>
      <li class="nav-item"><a href="#" class="nav-link px-2 text-muted">Pricing</a></li>
      <li class="nav-item"><a href="#" class="nav-link px-2 text-muted">FAQs</a></li>
      <li class="nav-item"><a href="#" class="nav-link px-2 text-muted">About</a></li>
    </ul>
    <p class="text-center text-muted">&copy; SunColleciton 2022, "Shop Forever Growth Together" </p>
  </footer>
</div>
        </div>

                </main>
            </div>
        </div>
    )
}

export default Dasboard