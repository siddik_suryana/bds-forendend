import React from "react";
import {
Box,
Container,
Row,
Column,
FooterLink,
Heading,
} from "./footerStyles";

const Footer = () => {
return (
	<Box>
	<h1 style={{ color: "green",
				textAlign: "center",
				marginTop: "-50px" }}>
	</h1>
	<Container>
		<Row>
		<Column>
			<Heading>About Us</Heading>
			<p>SUN COLLECTION E-commerce is the buying and selling</p> 
            <p>of goods or services via the internet, and the</p> 
            <p>transfer of money and data to complete the</p> 
            <p>sales. It’s also known as electronic</p> 
            <p>commerce or internet commerce.</p>
		</Column>
		<Column>
			<Heading>Products</Heading>
			<FooterLink href="#">Men's Clothing</FooterLink>
			<FooterLink href="#">Woman Clothing</FooterLink>
			<FooterLink href="#">Jewelery</FooterLink>
			<FooterLink href="#">Electronic</FooterLink>
		</Column>
		<Column>
			<Heading>Contact Us</Heading>
            {/* <FooterLink href="https://www.gmail.com"><FontAwesomeIcon icon="fa-regular fa-envelope" />suncolletion@gmail.com</FooterLink>
			<FooterLink href="https://www.instagram.com"><FontAwesomeIcon icon="fa-brands fa-instagram" />@suncolletion</FooterLink>
			<FooterLink href="https://www.facebook.com"><FontAwesomeIcon icon="fa-brands fa-facebook" />SunColletion</FooterLink> */}
		</Column>
		<Column>
			<Heading>Social Media</Heading>
			<FooterLink href="#">
			<i className="fab fa-facebook-f">
				<span style={{ marginLeft: "10px" }}>
				Facebook
				</span>
			</i>
			</FooterLink>
			<FooterLink href="#">
			<i className="fab fa-instagram">
				<span style={{ marginLeft: "10px" }}>
				Instagram
				</span>
			</i>
			</FooterLink>
			<FooterLink href="#">
			<i className="fab fa-twitter">
				<span style={{ marginLeft: "10px" }}>
				Twitter
				</span>
			</i>
			</FooterLink>
			<FooterLink href="#">
			<i className="fab fa-youtube">
				<span style={{ marginLeft: "10px" }}>
                    
				Youtube
				</span>
			</i>
			</FooterLink>
		</Column>
		</Row>
	</Container>
	</Box>
);
};
export default Footer;
