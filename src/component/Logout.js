import {GoogleLogin} from "react-google-login"
const clientId ="965657828139-3k87vahsm8j52oovmc8tt4ufurhptus9.apps.googleusercontent.com"

const LogoutG = () =>{

    const onSuccess = (res) =>{
        console.log("Logout Success! Current User : ", res.profileObj)
    }
    const onFailure = (res) =>{
        console.log("Logout Fiailure! Current User : ", res)
    }

    return(
        <GoogleLogin
    clientId={clientId}
    render={renderProps => (
      <button onClick={renderProps.onClick} disabled={renderProps.disabled}>This is my custom Google button</button>
    )}
    buttonText="Login"
    onSuccess={onSuccess}
    onFailure={onFailure}
    cookiePolicy={'single_host_origin'}
  />    
    )
}

export default LogoutG