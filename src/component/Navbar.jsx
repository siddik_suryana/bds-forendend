import React, { useEffect, useState, useContext } from 'react'
import {NavLink} from 'react-router-dom'
import Login from '../button/Login'
import Signup from '../button/Signup'
import { UserContext } from '../context/userContext';
import { useParams } from 'react-router-dom';
import {Redirect, Route, Link} from 'react-router-dom'
import axios from "axios"





function Navbar() {
    let { id } = useParams();
    const [data, setData] = useState([]);
    const [filter, setFilter] = useState(data);
    const [user, setUser] = useContext(UserContext)
    const [fetchTrigger, setFetchTrigger] = useState(true)
    


    let componentMounted = true;
    
    useEffect(() => {
        const fetchData = async () => {
            // setLoading(true)
            const resnponse = await fetch(`https://second-backend-heroku.herokuapp.com/cart-current-user`, { headers: { "Authorization": "Bearer " + user.token }})

            if (componentMounted) {
                setData(await resnponse.clone().json());
                setFilter(await resnponse.json());
                // setLoading(false);
                // console.log(filter)
            }
            return () => {
                componentMounted = false;
            }
            
        }
        if (fetchTrigger) {
            fetchData()
            setFetchTrigger(false)
        }
       
    },[fetchTrigger])

    const LoginRoute = ({...rest}) =>{
        if (user){
          return <Redirect to="/" />
        }else{
          return <Route {...rest}/>
        }
    }

    return (
        <div>
            {/* {console.log(filter.length)} */}
            
            <nav className="navbar navbar-expand-lg navbar-light bg-white py-3 shadow-sm">
                <div className="container">
                    <NavLink className="navbar-brand fw-bold fs-4 dark" to="/">
                    SUN COLLECTION</NavLink>
                    <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span className="navbar-toggler-icon"></span>
                    </button>
                    <div className="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul className="navbar-nav mx-auto mb-2 mb-lg-0">
                            <li className="nav-item">
                                <NavLink className="nav-link active" aria-current="page" to="/">Home</NavLink>
                            </li>
                            <li className="nav-item">
                                <NavLink className="nav-link" to="/products">Product</NavLink>
                            </li>
                            <li className="nav-item">
                                <NavLink className="nav-link" to="/about">About</NavLink>
                            </li>
                            <li className="nav-item">
                                <NavLink className="nav-link" to="/contact">Contact</NavLink>
                            </li>
                        </ul>
                        <Login/>
                        <Signup/>
                        { user !== null && (
           
                       <div className='buttons'>
                       
                           <NavLink to='/cart' className='btn btn-outline-dark ms-2'>
                           
                               <i className="fa fa-shopping-cart me-1"></i> Cart({filter.length})</NavLink>
                               
                       </div>
                       )}
                       
                    </div>
                </div>
            </nav>
        </div>
    )
}

export default Navbar