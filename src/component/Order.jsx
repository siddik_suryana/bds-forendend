import React, { useState, useEffect, useContext } from 'react'
import Skeleton from 'react-loading-skeleton'
import 'react-loading-skeleton/dist/skeleton.css'
import { NavLink } from 'react-router-dom'
import { useParams, useHistory, Link } from 'react-router-dom'
import NumberFormat from 'react-number-format'
import axios from 'axios';
import { UserContext } from '../context/userContext';





const Order = () => {
    const { id } = useParams()
    let history = useHistory();
    const [loading, setLoading] = useState(false)
    const [cart, setCart] = useState([])
    const [currentId, setCurrentId] = useState(null)
    const [fetchTrigger, setFetchTrigger] = useState(true)


    const [user] = useContext(UserContext)


    useEffect(() => {
        const fetchData = async () => {
            const result = await axios.get(`https://second-backend-heroku.herokuapp.com/cart-current-user`, { headers: { "Authorization": "Bearer " + user.token } })
            let data_cart = result.data
            setCart(data_cart)
            console.log(result.data)
            // setCurrentId(id)
        }
        if (fetchTrigger) {
            fetchData()
            setFetchTrigger(false)
        }
    }, [fetchTrigger])

    const Loading = () => {
        return (
            <>
                <div className="col-md-6">
                    <Skeleton height={400} />
                </div>
                <div className='col-md-6' style={{ lineHeihgt: 2 }}>
                    <Skeleton height={50} width={300} />
                    <Skeleton height={75} />
                    <Skeleton height={25} width={150} />
                    <Skeleton height={50} />
                    <Skeleton height={150} />
                    <Skeleton height={50} width={200} />


                </div>
            </>
        )
    }


    const handleSubmit = (event) => {
        event.preventDefault()

        axios.patch(`https://second-backend-heroku.herokuapp.com/shop/${currentId}`, cart, { headers: { "Authorization": "Bearer " + user.token } })
        setFetchTrigger(true)

        // setInput(initialForm)
    }

    // const handleChange = (event) => {
    //     let val = event.target.value
    //     let name = event.target.name
    //     setInput({ ...input, [name]: val })
    // }

    return (

        <div>
            <div class="container-fluid">
                <div class="row">
                    <nav id="sidebarMenu" class="col-md-3 col-lg-2 d-md-block bg-light sidebar collapse">
                        <div class="position-sticky pt-3">
                            <ul class="nav flex-column">
                                <li class="nav-item">
                                    <a class="nav-link " aria-current="page" href="#">
                                        <span data-feather="home"></span>
                                        <Link to="/order">
                                            <button class="fa fa-shopping-bag btn-outline-dark py-2 ms-auto w-100"> Order</button>
                                        </Link>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link " aria-current="page" href="#">
                                        <span data-feather="home"></span>
                                        <Link to="/dasboard">
                                            <button class="fa fa-minus btn btn-outline-dark ms-auto w-100"> Delete Products</button>
                                        </Link>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link active" aria-current="page" href="#">
                                        <span data-feather="home"></span>
                                        <Link to="/add">
                                            <button class="fa fa-plus btn btn-outline-dark ms-auto w-100"> Add Products</button>
                                        </Link>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link active" aria-current="page" href="#">
                                        <span data-feather="home"></span>
                                        <Link to="/update">
                                            <button class="fa fa-refresh btn btn-outline-dark ms-auto w-100"> Update Products</button>
                                        </Link>
                                    </a>
                                </li>

                            </ul>
                        </div>
                    </nav>

                    <main class="col-md-9 ms-sm-auto col-lg-10 px-md-4">
                        <div>
                            <div className="container my-5 py-3">
                                <div className="row">
                                    <div className="col-12 mb-5" >
                                        <h1 className='display-6 fw-bolder text-center' >Checking Order</h1>
                                        <hr />
                                    </div>

                                    <>
                                        <table class="table table-responsive table-hover table-bordered border-dark caption-top">
                                        <caption>List of Order</caption>
                                            <thead class="table-info table-bordered ">
                                                <tr>
                                                    <th scope="col">No</th>
                                                    <th scope="col">Full Name</th>
                                                    <th scope="col">Status</th>
                                                    <th scope="col">Total</th>
                                                    <th scope="col">Created At</th>
                                                    <th scope="col">Action</th>
                                                    
                                                </tr>
                                            </thead>
                                            <tbody>
                                            {cart.map((value, index)=>{
                return(

                    <tr key={index}>
                                                    <th scope="row">{index + 1}</th>
                                                    <td>{value.StructShop.title}</td>
                                                    <td>{value.Status}</td>
                                                    <td>{value.StructShop.price}</td>
                                                    <td>{value.created_at}</td>
                                                    <td><button></button></td>
                                                </tr>
    
               
                )
            })}
                                                
                                                
                                            </tbody>
                                        </table>
                                    </>


                                </div>


                            </div>
                            {/* footer */}
                            <div class="container">
                                <footer class="py-3 my-4">
                                    <ul class="nav justify-content-center border-bottom pb-3 mb-3">
                                        <li class="nav-item"><a href="#" class="nav-link px-2 text-muted">Home</a></li>
                                        <li class="nav-item"><a href="#" class="nav-link px-2 text-muted">Product</a></li>
                                        <li class="nav-item"><a href="#" class="nav-link px-2 text-muted">Pricing</a></li>
                                        <li class="nav-item"><a href="#" class="nav-link px-2 text-muted">FAQs</a></li>
                                        <li class="nav-item"><a href="#" class="nav-link px-2 text-muted">About</a></li>
                                    </ul>
                                    <p class="text-center text-muted">&copy; SunColleciton 2022, "Shop Forever Growth Together" </p>
                                </footer>
                            </div>
                        </div>

                    </main>
                </div>
            </div>
        </div>
    )
}

export default Order