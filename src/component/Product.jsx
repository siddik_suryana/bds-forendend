import React, { useState, useEffect, useContext } from 'react'
import { useDispatch } from 'react-redux'
import { addCart } from '../redux/action'
import Skeleton from 'react-loading-skeleton'
import 'react-loading-skeleton/dist/skeleton.css'
import { NavLink } from 'react-router-dom'
import { useParams, useHistory } from 'react-router-dom'
import NumberFormat from 'react-number-format';
import { UserContext } from '../context/userContext';
import axios from 'axios';
import { useForm } from 'react-hook-form';





const Product = () => {
    const { id } = useParams()
    const [product, setProduct] = useState([])
    const [loading, setLoading] = useState(false)
    let initialForm = { title: '', price: '' }
    const [input, setInput] = useState(initialForm)
    let initialForm1 = {id: '',email:'' }
    const [input1, setInput1] = useState(initialForm1)
    let history = useHistory();
    const [currentId, setCurrentId] = useState(null)
    const [currentUser, setCurrentUser] = useState(null)
    const [fetchTrigger, setFetchTrigger] = useState(true)
    const [user] = useContext(UserContext)

    const dispatch = useDispatch()
    const addProduct = () => {
        dispatch(addCart(product))


    }

    useEffect(() => {
        const fetchData = async () => {

            const result = await axios.get(`https://second-backend-heroku.herokuapp.com/shop/${id}`)
            let { title, price, category, Rating, image, description } = result.data
            setInput({ title, price, category, Rating, image, description })
            setCurrentId(id)
            const result_user = await axios.get(`https://second-backend-heroku.herokuapp.com/user`, { headers: { "Authorization": "Bearer " + user.token }})
            let id_user = result_user.data[0].id
            setInput1({id_user})
            

        }
        if (fetchTrigger) {
            fetchData()
            setFetchTrigger(false)
        }
    }, [fetchTrigger])

    const { handleSubmit } = useForm();
    const onSubmit = async (event) => {
        // event.preventDefault()
       
            // post

            const shop = await axios.post(`https://second-backend-heroku.herokuapp.com/shop/`, {}, { headers: { "Authorization": "Bearer " + user.token } })//input speart
            // const user = await axios.post(`https://second-backend-heroku.herokuapp.com/login/`, {}, { headers: { "Authorization": "Bearer " + user.token } })//input speart
            const cart = await axios.post(`https://second-backend-heroku.herokuapp.com/cart/`, { Status: "unpaid", StructShop_id: parseInt(currentId), user_id:input1.id_user}, { headers: { "Authorization": "Bearer " + user.token } })
            event = window.location.reload();
            
            
            
              

            // fetchTrigger(false)

        

        setInput1(initialForm1)
    }
    const handleChange = (event) => {
        let value = event.target.value
        let name = event.target.name

        //   setInput({ ...input, [name]: value })

        switch (name) {
            case "userID": {
                setInput1({ ...input1, userID: value })
                break;
            }
            case "status": {
                setInput({ ...input1, status: "sending" })
                break;
            }
            default: { break; }
        }
    }
    const Loading = () => {
        return (
            <>
                <div className="col-md-6">
                    <Skeleton height={400} />
                </div>
                <div className='col-md-6' style={{ lineHeihgt: 2 }}>
                    <Skeleton height={50} width={300} />
                    <Skeleton height={75} />
                    <Skeleton height={25} width={150} />
                    <Skeleton height={50} />
                    <Skeleton height={150} />
                    <Skeleton height={50} width={200} />


                </div>
            </>
        )
    }



    const ShowProduct = () => {
        return (
            <>
                <div className="col-md-6">
                    <img src={input.image} alt={input.title}
                        height='400px' width='400px' />
                    <form id="hook-form" onSubmit={handleSubmit(onSubmit)}>
                        <div class="row g-3">




                            {/* <div class="col-sm-6">
                    <label for="category" class="form-label">Product Category</label>
                    <input type="text" name="category" class="form-control" id="category" onChange={handleChange} value={product.category} placeholder="Category" required />
                    <div class="invalid-feedback">
                      Valid Category is required.
                    </div>
                  </div>

                  <div class="col-sm-6">
                    <label for="Rating" class="form-label">Product Rating</label>
                    <input type="text" name="Rating" class="form-control" id="Rating" onChange={handleChange} value={product.Rating} placeholder="Rating" required />
                    <div class="invalid-feedback">
                      Valid Rating is required.
                    </div>
                  </div>

                  <div class="col-12">
                    <label for="image" class="form-label">Image Url</label>
                    <div class="input-group has-validation">
                      <input type="text" class="form-control" name="image" id="image" onChange={handleChange} value={product.image} placeholder="Url" required />
                      <div class="invalid-feedback">
                        Your image is required.
                      </div>
                    </div>
                  </div>

                  <div class="col-12">
                    <label for="description" class="form-label">Description </label>
                    <textarea name='description' class="form-control" id="description" onChange={handleChange} value={product.description} rows="3"></textarea>
                    <div class="invalid-feedback">
                      Please enter a valid description.
                    </div>
                  </div> */}

                            {/* <!-- Modal --> */}
                            <div className="modal fade" id="addModal" tabIndex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div className="modal-dialog">
                                    <div className="modal-content">
                                        <div className="modal-header">
                                            <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                        </div>
                                        <div className="modal-body">
                                            <img src='/assets/checklist.png' style={{ width: "100%" }}></img>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                            <button className='btn btn-outline-dark px-4 py-2' type="submit" form="hook-form">
                                Add to Cart
                            </button>
                </div>
                <div className="col-md-6">
                    <h4 className='text-uppercase text-black-50'>
                        {input.category}
                    </h4>
                    <h1 className='display-5'>{input.title}</h1>
                    <p className='lead fw-bolder'>
                        Rating {input.Rating}
                        &nbsp;
                        <i className='fa fa-star' ></i>
                    </p>
                    <h3 className='display-6 fw-bold my-4'>
                        Rp<NumberFormat value={input.price} displayType={'text'} thousandSeparator={true} prefix={' '} />
                    </h3>
                    <p className='lead'>{input.description}</p>


                    <NavLink to='/checkout' className='btn btn-outline-dark ms-2 px-3 py-2'  >
                        Go to Cart
                    </NavLink>

                </div>
            </>
        )
    }

    return (
        <div>
            <div className="container py-4">
                <div className="row">
                    {loading ? <Loading /> : <ShowProduct />}
                </div>
            </div>
        </div>
    )
}

export default Product