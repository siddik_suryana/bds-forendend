import React, { useEffect, useState, } from 'react'
import Skeleton from "react-loading-skeleton"
import 'react-loading-skeleton/dist/skeleton.css'
import { NavLink } from 'react-router-dom';
import NumberFormat from 'react-number-format';
// import Footer from './Footer';
// import axios from "axios"



const Products = () => {
    const [data, setData] = useState([]);
    const [filter, setFilter] = useState(data);
    const [loading, setLoading] = useState(false);
    // const [fetchTrigger, setFetchTrigger] = useState(true)
    let componentMounted = true;
    
    useEffect(() => {
        const fetchData = async () => {
            setLoading(true)
            const resnponse = await fetch("https://second-backend-heroku.herokuapp.com/shop")
            if (componentMounted) {
                setData(await resnponse.clone().json());
                setFilter(await resnponse.json());
                setLoading(false);
                
            }
            return () => {
                componentMounted = false;
            }
            
        }
        fetchData()  
    },[])
    

    const Loading = () => {
        return (
            <>
                <div className='display-6 fw-bolder text-center'>
                    <Skeleton height={20} width={500} />
                </div>

                <>
                    <div className="col-md-3">
                        <Skeleton height={350} />
                    </div>
                    <div className="col-md-3">
                        <Skeleton height={350} />
                    </div>
                    <div className="col-md-3">
                        <Skeleton height={350} />
                    </div>
                    <div className="col-md-3">
                        <Skeleton height={350} />
                    </div>
                </>
            </>
        )
    }

    const filterProduct = (cat) => {
        const updateList = data.filter((x) => x.category === cat)
        setFilter(updateList)
    }

   
    const ShowProducts = () => {
        return (
            <>
                <div className='buttons d-flex justify-content-center mb-5 pb-5'>
                    <button className='btn btn-outline-dark me-2' onClick={() => setFilter(data)}>All</button>
                    <button className='btn btn-outline-dark me-2' onClick={() => filterProduct("men's clothing")}>Men's Clothing</button>
                    <button className='btn btn-outline-dark me-2' onClick={() => filterProduct("women's clothing")}>Woman's Clothing</button>
                    <button className='btn btn-outline-dark me-2' onClick={() => filterProduct("jewelery")}>Jewelery</button>
                    <button className='btn btn-outline-dark me-2' onClick={() => filterProduct("electronics")}>Electronic</button>
                </div>
                {filter.map((products) => {
                    return (
                        <>
                            
                                <div class="card my-2 mx-2 py-4" style={{width:"14rem"}} key={products.id}>
                                    <img src={products.image} class="card-img-top" alt={products.title} height="220px" />
                                    <div class="card-body text-center">
                                        <h5 class="card-title mb-0">{products.title.substring(0, 11)}..</h5>
                                        <p class="card-text lead fw-bold">Rp<NumberFormat value={products.price} displayType={'text'} thousandSeparator={true} prefix={' '} /></p>
                                        <NavLink to={`/products/${products.id}`} class="btn btn-outline-dark">Buy Now</NavLink>
                                    </div>
                                </div>
                            
                        </>
                    )
                })}
            </>
        )
    }

    return (
        <div>
            <div className="container my-5 py-5">
                <div className="row">
                    <div className="col-12 mb-5" >
                        <h1 className='display-6 fw-bolder text-center' >Latest Products</h1>
                        <hr />
                    </div>
                </div>
                <div className="row justify-content-center">
                    {loading ? <Loading /> : <ShowProducts />}
                </div>
            </div>
            <div class="container">
  <footer class="py-3 my-4">
    <ul class="nav justify-content-center border-bottom pb-3 mb-3">
      <li class="nav-item"><a href="#" class="nav-link px-2 text-muted">Home</a></li>
      <li class="nav-item"><a href="#" class="nav-link px-2 text-muted">Product</a></li>
      <li class="nav-item"><a href="#" class="nav-link px-2 text-muted">Pricing</a></li>
      <li class="nav-item"><a href="#" class="nav-link px-2 text-muted">FAQs</a></li>
      <li class="nav-item"><a href="#" class="nav-link px-2 text-muted">About</a></li>
    </ul>
    <p class="text-center text-muted">&copy; SunColleciton 2022, "Shop Forever Growth Together" </p>
  </footer>
</div>
        </div>
    )
}

export default Products

