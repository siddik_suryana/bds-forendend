import {GoogleLogin} from "react-google-login"

const clientId ="965657828139-3k87vahsm8j52oovmc8tt4ufurhptus9.apps.googleusercontent.com"

const LoginG = () =>{

    const onSuccess = (res) =>{
        console.log("Login Success! Current User : ", res.profileObj)
    }
    const onFailure = (res) =>{
        console.log("Login Success! Current User : ", res)
    }

    return(
        <GoogleLogin
    clientId={clientId}
    render={renderProps => (
        

      <button onClick={renderProps.onClick} className="fa fa-google btn btn-outline-dark opacity-90 w-100" disabled={renderProps.disabled}> Login With Google</button>
    )}
    buttonText="Login"
    onSuccess={onSuccess}
    onFailure={onFailure}
    cookiePolicy={'single_host_origin'}
  />
    )
}

export default LoginG