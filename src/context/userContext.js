import React, { useState, createContext, useEffect } from "react";
import axios from 'axios';


export const UserContext = createContext();

export const UserProvider = props => {
  const currentUser = JSON.parse(localStorage.getItem("user"))
  const [user, setUser] = useState(currentUser);

  useEffect(() => {

    const fetchData = async () => {
      const result = await axios.get(`https://backend-fullstack-heroku.herokuapp.com/data-shopping/current-user`, { headers: { "Authorization": "Bearer " + user.token } })
      let shoppings = result.data
      

      //ngeluarin status
      let findShoppingCart = shoppings.find((item)=>{
        return item.status == "cart"
      }) 
  
      // validasi status 
      if (findShoppingCart != undefined){
        user.shoppingID = findShoppingCart.id
      } else {
        user.shoppingID = null
      }
      
      setUser(user)
    }
    if (user != null) {
      if (user.shoppingID == undefined) {
       fetchData()
      }
    }
  },[user])

  return (
    <UserContext.Provider value={[user, setUser]}>
      {props.children}
    </UserContext.Provider>
  );
};